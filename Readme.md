# Commands

## Start dev version of program
    npm run dev

## Build
    npm build

## Start Pre-built project
    npm start

## Run tests
    npm test
