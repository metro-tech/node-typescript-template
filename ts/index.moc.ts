import {expect} from 'chai';
import main from './index';
import util from 'util';

let outputString = '' ;

// always use function ...() with mocha, do not use arrow functions if you
// might need to set a mocha property, e.g. timeout
before(async function globalBeforeHook(): Promise<void> {
    // this.timeout would not work if this was an arrow function
    this.timeout(10000);
});

after(async function globalAfterHook(): Promise<void> {
    //cleanup code here
});

describe('test main()', function() {
    it('Check console output', async function () {
        // setting console to output to a string
        const oldConsole = console.log;
        console.log = function(...args): void {
            outputString = outputString + util.format.apply(null, args) + '\n';
        };

        try {
            await main();
        } catch (err) {
            console.log = oldConsole;
            throw err;
        }

        console.log = oldConsole;

        expect(outputString.startsWith('Hello, the current time is ')).to.be.true;
    });
});
