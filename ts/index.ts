import dayjs from 'dayjs';
import localizedFormat from 'dayjs/plugin/localizedFormat';

dayjs.extend(localizedFormat);

async function main(): Promise<void> {
    console.log(`Hello, the current time is ${dayjs().format('LLLL')}.`);
}

if (require.main === module) {
    main().catch();
}

export default main;
