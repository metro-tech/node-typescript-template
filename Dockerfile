FROM node:hydrogen-alpine as builder
WORKDIR /opt/metropolis
COPY . .
RUN npm install
RUN npm run build

FROM node:hydrogen-alpine
WORKDIR /opt/metropolis
COPY package*.json ./
RUN npm install --production

COPY --from=builder /opt/metropolis/dist/ /opt/metropolis/dist/
CMD npm start
